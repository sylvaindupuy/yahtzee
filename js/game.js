var dice, diceToReroll, turnNumber, SCORE, nbSelected

$(document).ready(function () {
    diceValues = new Array(5)
    diceValues.fill(0)
    diceToReroll = [0, 1, 2, 3, 4]
    turnNumber = 0
    nbSelected = 0
    SCORE = 0
    $('#rollTheDice').click(rollDice)
    $('.clickable').click(selectCombo)
    $('#saveScore').attr("disabled", true)
});

// On renvoie un nombre aléatoire entre une valeur min (incluse) 
// et une valeur max (incluse)
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function add(a, b) {
    return a + b
}

function rollDice() {
    turnNumber++;
    $('#turn').text("Tour n°" + turnNumber.toString())
    // clear the old combo and dice
    clearCombo()
    clearDice()
    // for all the dice
    for (var dice = 0; dice < 5; dice++) {
        // if we reroll
        if (diceToReroll.includes(dice)) {
            diceValues[dice] = getRandomInt(1, 6)
            if (turnNumber == 1) {
                $('#playerDice tbody tr').append('<td><img class="dice" placement="' + dice + '" src="./images/' + diceValues[dice] + '.png"></td>')
            } else {
                $('#playerDice tr td:eq(' + dice + ')').children().first().attr('src', './images/' + diceValues[dice] + '.png').removeClass('diceToReroll')
            }
        }
        // add the dice to score
        if (!$('#score' + diceValues[dice]).hasClass("used"))
            $('#score' + diceValues[dice]).text((parseInt($('#score' + diceValues[dice]).text()) + diceValues[dice]).toString())
    }
    // Update the chance score
    if (!$('#chance').hasClass("used"))
        $('#chance').text(diceValues.reduce(add, 0))

    if (turnNumber < 3) {
        $('#rollTheDice').text('Choisissez les dés à relancer !').attr("disabled", true)
    } else if (turnNumber == 3) {
        $('#rollTheDice').text('Vous ne pouvez plus relancer, choisissez une case !')
        $('#rollTheDice').attr("disabled", true)
    }
    // add cick event on dice
    $('.dice').unbind().click(selectOrDeselectDice)
    // Clear the dice to reroll array
    diceToReroll.splice(0, diceToReroll.length)
    // add combo Search
    comboSearch()
}

function clearCombo() {
    $('.combo').each(function () {
        if (!$(this).hasClass("used"))
            $(this).text("0")
    })
}

function clearDice() {
    $("[id^=score]").each(function () {
        if (!$(this).hasClass("used"))
            $(this).text("0")
    })
}

function comboSearch() {
    // we create an array of occurrences
    var occurrences = new Array(6)
    var largerSuite = 0
    var currentSuite = 0
    var numberOfPairs = 0
    var brelan = false
    // we initialize it
    occurrences.fill(0)
    // we fill it
    diceValues.forEach(dice => {
        occurrences[dice - 1] += 1
    })
    occurrences.forEach((occur, i) => {
        if (occur == 2) {
            numberOfPairs++
        }
        if (occur >= 3) {
            if (!$('#brelan').hasClass("used")) {
                $('#brelan').text(diceValues.reduce(add, 0))
            }
            brelan = true
        }
        if (occur >= 4) {
            if (!$('#brelan').hasClass("used")) {
                $('#carre').text(diceValues.reduce(add, 0))
            }
        }
        if (occur == 5) {
            if (!$('#brelan').hasClass("used")) {
                $('#yahtzee').text("50")
            }
        }
        // used to detect suite
        currentSuite = (occur == 0) ? 0 : currentSuite + 1
        largerSuite = (currentSuite > largerSuite) ? currentSuite : largerSuite
    })
    // check if we have suite
    if (largerSuite >= 4)
        if (!$('#brelan').hasClass("used")) {
            $('#petiteSuite').text("30")
        }
    if (largerSuite == 5)
        if (!$('#brelan').hasClass("used")) {
            $('#grandeSuite').text("40")
        }
    // Check if we have a Full
    if (brelan && numberOfPairs == 1)
        if (!$('#brelan').hasClass("used")) {
            $('#full').text("25")
        }
}

function selectOrDeselectDice() {
    if (turnNumber < 3) {
        var $dice = $(this);
        var placement = $dice.attr('placement')
        var index = diceToReroll.indexOf(parseInt(placement))
        $dice.toggleClass('diceToReroll')
        if (index == -1) {
            diceToReroll.push(parseInt(placement))
        } else {
            diceToReroll.splice(index, 1)
        }
        if (diceToReroll.length == 1) {
            $('#rollTheDice').text('relancer les dès !').attr("disabled", false)
        } else if (diceToReroll.length == 0) {
            $('#rollTheDice').text('Choisissez les dés à relancer !').attr("disabled", true)
        }
    }
}

function saveScore() {
    if ($('#username').val()) {
        var nbScores = $.cookie("nbScores")
        var score = new Array($('#username').val(), SCORE.toString())
        if (nbScores == undefined) {
            nbScores = 0
        }
        nbScores = parseInt(nbScores) + 1
        var cookieName = "score" + nbScores
        $.cookie("nbScores", nbScores)
        $.cookie(cookieName, score.join(','));
        $('#saveScore').hide()
        $('#username').hide()
        window.open('./scores.html', 'les scores !');
    }
}

function selectCombo() {
    selectScore = $(this)
    if (turnNumber != 0 && !selectScore.hasClass("used")) {
        nbSelected++
        SCORE += parseInt(selectScore.text())
        selectScore.css("background-color", "rgb(220, 225, 184)");
        selectScore.prop("onclick", null).addClass("used")
        $('#playerDice tbody tr').empty()
        clearCombo()
        clearDice()
        diceToReroll.splice(0, diceToReroll.length)
        diceValues.splice(0, diceValues.length)
        diceToReroll = [0, 1, 2, 3, 4]
        $('#total').text(SCORE.toString())
        if (nbSelected != 13) {
            turnNumber = 0
            $('#rollTheDice').text('Lancer les dès !')
            $('#rollTheDice').attr("disabled", false)
        } else {
            // its finish
            $('#saveScore').attr("disabled", false)
            $('#saveScore').click(saveScore)
            $('#rollTheDice').text('Rejouer !')
            $('#rollTheDice').attr("disabled", false)
            $('#rollTheDice').unbind().click(refreshPage)
        }
    }
}

function refreshPage() {
    location.reload();
}