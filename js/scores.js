$(document).ready(function () {
    var nbScores = $.cookie("nbScores")
    if(nbScores == undefined){
        $('body').append('<h1>Aucun score n\'a été sauvegardé</h1>')
    }
    else
    {
        $('#divToShow').show()
        for(var i =1;i<=nbScores;i++){
            var score =  $.cookie("score"+i).split(/,/);
            $('#scores').append('<li>'+score[0]+' avec un score de '+score[1]+'</li>')
        }
    }
});